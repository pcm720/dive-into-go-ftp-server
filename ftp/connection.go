package ftp

import (
	"bufio"
	"context"
	"log"
	"net"
	"strings"
	"sync"
	"time"
	"unicode"
)

type Connection struct {
	transferType transferType

	ctx       context.Context
	timerCtrl chan timerControl

	ctrlConnection  net.Conn
	dataConnection  *dataConnection
	transferOngoing bool
	dataMutex       sync.Mutex // to prevent more than one data connections at the same time while being able to respond to control commands

	rootPath   string
	currentDir string // relative to rootPath
}

func HandleConnection(c net.Conn, timeout time.Duration, rootPath string) {
	log.Printf("incoming connection from %s", c.RemoteAddr())
	ctx, cancel := context.WithCancel(context.Background())
	conn := Connection{
		ctx:            ctx,
		timerCtrl:      make(chan timerControl),
		ctrlConnection: c,
		rootPath:       rootPath,
	}
	defer conn.ctrlConnection.Close()

	go func() { // timeout timer
		t := time.NewTimer(timeout)
		for {
			select {
			case action := <-conn.timerCtrl:
				switch action {
				case tStop:
					t.Stop()
					cancel()
					return
				case tPause:
					t.Stop()
				case tReset:
					t.Reset(timeout)
				}
			case <-t.C:
				t.Stop()
				cancel()
				return
			}
		}
	}()

	log.Printf("closing connection from %s (%s)", c.RemoteAddr(), conn.start())
	conn.timerCtrl <- tStop
}

func (c *Connection) start() reason {
	c.writeResponse(s220_ServiceReady)

	s := bufio.NewScanner(c.ctrlConnection)

	cmdChan := make(chan string)
	go func(ch chan string) {
		for s.Scan() {
			ch <- s.Text()
		}
	}(cmdChan)

	for {
		select {
		case <-c.ctx.Done():
			c.writeResponse(s421_NotAvailable)
			return timeout
		case cmd := <-cmdChan:
			if !c.transferOngoing {
				c.timerCtrl <- tReset
			}

			input := strings.Fields(cmd)
			if len(input) == 0 {
				continue
			}

			// converting to uppercase since commands need to be valid even if they're sent in mixed case (ex. lIsT)
			command := strings.ToUpper(strings.TrimFunc(input[0], func(r rune) bool {
				// some clients send junk bytes before the command (e.g. GNU inetutils FTP client sending extra bytes when transfer is aborted with Ctrl+C)
				return !unicode.IsLetter(r)
			}))

			switch command {
			case "ABOR":
				if c.dataConnection != nil {
					c.dataConnection.abort()
					continue
				}
				c.writeResponse(s226_DataClosing)
			case "CWD":
				c.cmdCwd(input[1:])
			case "DELE":
				c.cmdDele(input[1:])
			case "LIST":
				c.cmdList(input[1:], false)
			case "MODE":
				c.cmdMode(input[1:])
			case "NLST":
				c.cmdList(input[1:], true)
			case "NOOP":
				c.writeResponse(s200_CmdOk)
			case "PASS":
				c.writeResponse(s202_Superfluous)
			case "PASV":
				c.newPassiveConnection()
			case "PORT":
				c.newActiveConnection(input[1:])
			case "QUIT":
				c.writeResponse(s221_ClosingConnection)
				return quit
			case "RETR":
				c.cmdRetr(input[1:])
			case "STOR":
				c.cmdStor(input[1:])
			case "STRU":
				c.cmdStru(input[1:])
			case "TYPE":
				c.cmdType(input[1:])
			case "USER":
				c.cmdUser(input[1:])
			default:
				c.writeResponse(s502_NotImplemented)
			}
		}
	}
}

func (c *Connection) writeResponse(resp string) {
	switch c.transferType {
	case ascii:
		resp += "\r\n"
	default:
		resp += "\n"
	}

	c.ctrlConnection.Write([]byte(resp))
}
