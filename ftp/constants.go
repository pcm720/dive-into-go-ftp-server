package ftp

type transferType int

const (
	ascii transferType = iota
	binary
)

type reason int

const (
	none reason = iota
	timeout
	quit
)

func (r reason) String() string {
	switch r {
	case timeout:
		return "timeout"
	case quit:
		return "client issued quit command"
	}
	return ""
}

type dataConnType int

const (
	dNone dataConnType = iota
	passive
	active
)

type timerControl int

const (
	tNone timerControl = iota
	tStop
	tPause
	tReset
)
