package ftp

import (
	"context"
	"errors"
	"io"
	"log"
	"net"
	"time"
)

type dataConnection struct {
	connType dataConnType

	listener net.Listener
	address  string
	conn     net.Conn

	transferContext context.Context
	cancelTransfer  context.CancelFunc
}

func (d *dataConnection) cleanup() {
	if d.connType == passive {
		d.listener.Close()
	}
}

func (d *dataConnection) close() {
	d.conn.Close()
}

func (d *dataConnection) abort() {
	d.cancelTransfer()
	d.close()
}

func (d *dataConnection) start() error {
	if d.connType == dNone {
		return errors.New("connection not initialized")
	}

	var err error
	if d.connType == passive {
		d.conn, err = d.listener.Accept()
		if err != nil {
			log.Printf("failed to accept passive connection: %s", err)
			return err
		}
		log.Printf("accepted data connection from %s", d.conn.RemoteAddr().String())
		return nil
	}

	// active
	d.conn, err = net.DialTimeout("tcp", d.address, 5*time.Second)
	if err != nil {
		log.Printf("failed to open active data connection: %s", err)
		return err
	}
	log.Printf("opened data connection to %s", d.conn.RemoteAddr().String())
	return nil
}

// Reader interface implementation
func (d *dataConnection) Read(data []byte) (int, error) {
	select {
	case <-d.transferContext.Done():
		return 0, errors.New("aborted")
	default:
		return d.conn.Read(data)
	}
}

// Writer interface implementation
func (d *dataConnection) Write(data []byte) (int, error) {
	select {
	case <-d.transferContext.Done():
		return 0, errors.New("aborted")
	default:
		return d.conn.Write(data)
	}
}

// ReaderFrom interface implementation for io.Copy
func (d *dataConnection) ReadFrom(data io.Reader) (int64, error) {
	buffer := make([]byte, 32768)
	var totalBytesWritten int64
	for {
		select {
		case <-d.transferContext.Done():
			return totalBytesWritten, errors.New("aborted")
		default:
			bytesRead, err := data.Read(buffer)
			if err == io.EOF {
				goto out
			}
			if err != nil {
				return totalBytesWritten, err
			}

			if _, err := d.conn.Write(buffer[:bytesRead]); err != nil {
				return totalBytesWritten, err
			}
			totalBytesWritten += int64(bytesRead)
		}
	}
out:
	return totalBytesWritten, nil
}
