package ftp

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func (c *Connection) cmdUser(args []string) {
	if len(args) < 1 {
		c.writeResponse(s501_InvalidArgument)
		return
	}

	c.writeResponse(fmt.Sprintf(s230_LoggedIn, strings.Join(args, " ")))
}

func (c *Connection) cmdType(args []string) {
	if len(args) < 1 {
		c.writeResponse(s501_InvalidArgument)
		return
	}

	switch args[0] {
	case "A":
		c.transferType = ascii
	case "I":
		c.transferType = binary
	default:
		c.writeResponse(s504_ArgNotImplemented)
		return
	}
	c.writeResponse(s200_CmdOk)
}

func (c *Connection) cmdMode(args []string) {
	if len(args) < 1 {
		c.writeResponse(s501_InvalidArgument)
		return
	}

	if args[0] == "S" {
		c.writeResponse(s200_CmdOk)
	} else {
		c.writeResponse(s504_ArgNotImplemented)
	}
}

func (c *Connection) cmdStru(args []string) {
	if len(args) < 1 {
		c.writeResponse(s501_InvalidArgument)
		return
	}

	if args[0] == "F" {
		c.writeResponse(s200_CmdOk)
	} else {
		c.writeResponse(s504_ArgNotImplemented)
	}
}

func (c *Connection) cmdCwd(args []string) {
	if len(args) != 1 {
		c.writeResponse(s501_InvalidArgument)
		return
	}

	newCurrentDir := filepath.Join(c.currentDir, args[0])
	path := filepath.Join(c.rootPath, newCurrentDir)

	if !strings.HasPrefix(path, c.rootPath) { // won't go past the root directory
		c.writeResponse(s550_ActionNotTaken)
		return
	}

	if _, err := os.Stat(path); err != nil {
		c.writeResponse(s550_ActionNotTaken)
		return
	}
	c.currentDir = newCurrentDir
	c.writeResponse(s200_CmdOk)
}

func (c *Connection) cmdDele(args []string) {
	if len(args) != 1 {
		c.writeResponse(s501_InvalidArgument)
		return
	}

	path := filepath.Join(c.rootPath, c.currentDir, args[0])

	if !strings.HasPrefix(path, c.rootPath) { // won't go past the root directory
		c.writeResponse(s550_ActionNotTaken)
		return
	}

	if err := os.Remove(path); err != nil {
		c.writeResponse(s550_ActionNotTaken)
		return
	}
	c.writeResponse(s200_CmdOk)
}

func (c *Connection) cmdList(args []string, shortOutput bool) {
	c.dataMutex.Lock()
	c.transferOngoing = true
	defer func() {
		c.transferOngoing = false
		c.dataMutex.Unlock()
	}()

	path := filepath.Join(c.rootPath, c.currentDir)
	if len(args) > 0 {
		path = filepath.Join(path, args[0])
	}

	if !strings.HasPrefix(path, c.rootPath) { // won't go past the root directory
		c.writeResponse(s550_ActionNotTaken)
		return
	}

	dirList, err := ioutil.ReadDir(path)
	if err != nil {
		c.writeResponse(s550_ActionNotTaken)
	}

	c.writeResponse(s150_AboutToOpen)
	if c.dataConnection == nil {
		c.writeResponse(s426_Aborted)
		return
	}

	go func() {
		if err = c.dataConnection.start(); err != nil {
			c.writeResponse(s425_DataFail)
			return
		}
		defer c.dataConnection.close()

		c.timerCtrl <- tPause

		eol := "\r\n"
		if c.transferType == binary {
			eol = "\n"
		}

		for _, item := range dirList {
			line := ""
			if shortOutput {
				line = item.Name() + eol
			} else {
				line = fmt.Sprintf("%s\t%d\t%s\t%s"+eol, item.Mode().String(), item.Size(), item.ModTime().Format("Jan 2 15:04"), item.Name())
			}
			if _, err := c.dataConnection.Write([]byte(line)); err != nil {
				c.writeResponse(s426_Aborted)
				break
			}
		}
		c.writeResponse(s226_DataClosing)
		c.timerCtrl <- tReset
	}()
}

func (c *Connection) cmdRetr(args []string) {
	c.dataMutex.Lock()
	c.transferOngoing = true
	defer func() {
		c.transferOngoing = false
		c.dataMutex.Unlock()
	}()

	if len(args) != 1 {
		c.writeResponse(s501_InvalidArgument)
		return
	}

	path := filepath.Join(c.rootPath, c.currentDir, args[0])
	if !strings.HasPrefix(path, c.rootPath) { // won't go past the root directory
		c.writeResponse(s550_ActionNotTaken)
		return
	}

	file, err := os.Open(path)
	if err != nil {
		c.writeResponse(s550_ActionNotTaken)
		return
	}

	c.writeResponse(s150_AboutToOpen)
	if c.dataConnection == nil {
		c.writeResponse(s426_Aborted)
		return
	}

	go func(file *os.File) {
		defer file.Close()

		if err = c.dataConnection.start(); err != nil {
			c.writeResponse(s425_DataFail)
			return
		}
		defer c.dataConnection.close()

		c.timerCtrl <- tPause
		_, err = io.Copy(c.dataConnection, file)
		if err != nil {
			c.writeResponse(s426_Aborted)
		}
		c.writeResponse(s226_DataClosing)
		c.timerCtrl <- tReset
	}(file)
}

func (c *Connection) cmdStor(args []string) {
	c.dataMutex.Lock()
	c.transferOngoing = true
	defer func() {
		c.transferOngoing = false
		c.dataMutex.Unlock()
	}()

	if len(args) != 1 {
		c.writeResponse(s501_InvalidArgument)
		return
	}

	path := filepath.Join(c.rootPath, c.currentDir, args[0])
	if !strings.HasPrefix(path, c.rootPath) { // won't go past the root directory
		c.writeResponse(s550_ActionNotTaken)
		return
	}

	file, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		c.writeResponse(s550_ActionNotTaken)
		return
	}

	c.writeResponse(s150_AboutToOpen)
	if c.dataConnection == nil {
		c.writeResponse(s426_Aborted)
		return
	}

	go func(file *os.File) {
		defer file.Close()

		if err = c.dataConnection.start(); err != nil {
			c.writeResponse(s425_DataFail)
			return
		}
		defer c.dataConnection.close()

		c.timerCtrl <- tPause
		_, err = io.Copy(file, c.dataConnection)
		if err != nil {
			c.writeResponse(s426_Aborted)
		}
		c.writeResponse(s226_DataClosing)
		c.timerCtrl <- tReset
	}(file)
}
