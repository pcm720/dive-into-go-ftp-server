package ftp

import (
	"context"
	"fmt"
	"math/rand"
	"net"
	"strconv"
	"strings"
	"time"
)

func (c *Connection) newPassiveConnection() {
	if c.dataConnection != nil && c.dataConnection.connType != dNone {
		c.dataConnection.cleanup()
	}

	var (
		port int
		ln   net.Listener
		err  error
	)

	r := rand.New(rand.NewSource(time.Now().Unix()))
	for i := 0; i < 10; i++ { // try to listen on random port, retry for 10 times in a row in case random port is already used
		port = 1024 + r.Intn(64511) // use non-privileged ports
		if ln, err = net.Listen("tcp4", fmt.Sprintf(":%d", port)); err == nil {
			break
		}
	}
	if err != nil {
		c.writeResponse(s425_DataFail)
		return
	}

	address := c.ctrlConnection.LocalAddr().String() // use control connection listener address

	// convert address to "oct1,oct2,oct3,oct4,port1,port2"
	portIndex := strings.LastIndex(address, ":")
	address = strings.ReplaceAll(address[:portIndex], ".", ",")
	address += "," + strconv.Itoa(port>>8&0xFF) + "," + strconv.Itoa(port&0xFF)

	ctx, cancel := context.WithCancel(context.Background())
	c.dataConnection = &dataConnection{
		connType: passive,

		listener: ln,
		address:  address, // store prepared address string for connection reuse

		transferContext: ctx,
		cancelTransfer:  cancel,
	}
	c.writeResponse(fmt.Sprintf(s227_PassiveMode, address))
}

func (c *Connection) newActiveConnection(args []string) {
	if c.dataConnection != nil && c.dataConnection.connType != dNone {
		c.dataConnection.cleanup()
	}

	if len(args) < 1 {
		c.writeResponse(s501_InvalidArgument)
		return
	}

	// extract address and port number from "oct1,oct2,oct3,oct4,port1,port2"
	addr := strings.Split(args[0], ",")
	if len(addr) != 6 {
		c.writeResponse(s501_InvalidArgument)
		return
	}

	port1, err := strconv.Atoi(addr[4])
	if err != nil {
		c.writeResponse(s501_InvalidArgument)
		return
	}
	port2, err := strconv.Atoi(addr[5])
	if err != nil {
		c.writeResponse(s501_InvalidArgument)
		return
	}
	port1 = port1<<8 + port2

	ctx, cancel := context.WithCancel(context.Background())
	c.dataConnection = &dataConnection{
		connType: active,

		address: strings.Join(addr[:4], ".") + ":" + strconv.Itoa(port1),

		transferContext: ctx,
		cancelTransfer:  cancel,
	}
	c.writeResponse(s200_CmdOk)
}
