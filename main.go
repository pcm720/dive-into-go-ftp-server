package main

import (
	"flag"
	"ftp-server/ftp"
	"log"
	"net"
	"os"
	"path/filepath"
	"time"
)

var (
	ip       = flag.String("a", "", "Address to listen on")
	port     = flag.String("p", "8021", "Port to listen on")
	timeout  = flag.Int("t", 300, "Timeout for control connection in seconds")
	rootPath = flag.String("path", "", "Root path (default is current working directory)")
)

func init() {
	flag.Parse()
}

func main() {
	ln, err := net.Listen("tcp4", *ip+":"+*port)
	if err != nil {
		log.Fatalf("failed to bind to port: %s", err)
	}

	if *rootPath == "" {
		cwd, err := os.Getwd()
		if err != nil {
			log.Fatalf("can't get current working directory: %s", err)
		}
		rootPath = &cwd
	} else {
		absPath, err := filepath.Abs(*rootPath)
		if err != nil {
			log.Fatalf("can't get absolute path")
		}
		rootPath = &absPath
	}

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println("can't accept new connection")
		}
		go ftp.HandleConnection(conn, time.Duration(*timeout)*time.Second, *rootPath)
	}
}
